package exercise_java_basic;

import java.util.concurrent.ThreadLocalRandom;

public class Exercise1 {

	
	/*
	 * 1.a Write a program where a integer array (not list) 
	 * gets 16 random numbers between 0 and 100. (Use the method given to you)
	 * 1.b create a method that calculates the average of the numbers in that list
	 * 1.c create a method that finds the highest number
	 * 1.d create a method that finds the lowest number
	 * 1.e create a print method for the array, and make it look like the example
	 * example print : 
	 */												
	
	public static void main(String[] args) {

	}
	//given to you for free
	public static int randomInteger() {
		return ThreadLocalRandom.current().nextInt(0, 100);
	}
	
	public static int getMin(int[]anArray) {
		return 0;
	}
	
	public static int getMax(int[]anArray) {
		return 0;
	}
	
	public static int getAverage(int []anArray) {
		return 0;
	}
	
	public static void print(int[]anArray) {

	}
	
	
}
